#ifndef LOGGER_LOGGER_H
#define LOGGER_LOGGER_H

#include <iostream>
#include <filesystem>
#include <sstream>
#include <fstream>

class Logger {
    constexpr static std::string_view RED = "\033[38;5;196m", ORANGE = "\033[38;5;220m", WHITE = "\033[38;5;252m", RESET = "\033[0m";
    constexpr static std::string_view ERROR_LOG_PREFIX = "[ ERROR ] ", WARNING_LOG_PREFIX = "[WARNING] ", INFO_LOG_PREFIX = "[  LOG  ] ";
    constexpr static std::string_view END_LINE = "\n", SEPARATOR = " - ";

    enum class LogLevel : char {
        ErrorLog, WarningLog, InfoLog
    };

    inline static std::stringstream _log_message;
    inline static std::ofstream _outputFile;
    inline static bool _logTime = true;

    template<typename T>
    static void Log(const Logger::LogLevel &logLevel, T const &log_text) {
        std::string_view prefix, color;
        switch (logLevel) {
            case LogLevel::ErrorLog:
                color = RED;
                prefix = ERROR_LOG_PREFIX;
                break;
            case LogLevel::WarningLog:
                color = ORANGE;
                prefix = WARNING_LOG_PREFIX;
                break;
            case LogLevel::InfoLog:
                color = WHITE;
                prefix = INFO_LOG_PREFIX;
                break;
            default:
                return;
        }
        _log_message.seekp(0, std::ios::beg);
        _log_message << prefix;
        if (_logTime) {
            const std::time_t t = std::time(nullptr);
            tm temp_tm;
            char time_str[20];
            std::strftime(time_str, sizeof(time_str), "%Y/%m/%d %H:%M:%S", localtime_r(&t, &temp_tm));
            _log_message << time_str << SEPARATOR;
        }
        _log_message << log_text;
#if __cplusplus >= 202002L
        WriteToStreams(color, _log_message.view().substr(0, _log_message.tellp()));
#else //not C++ 20
        WriteToStreams(color, std::string_view(_log_message.str().c_str(), _log_message.tellp()));
#endif //endif C++ 20
    }

    static void WriteToStreams(const std::string_view &color, const std::string_view &log_message) {
        std::cout << color << log_message << RESET << END_LINE;
        if (!_outputFile.is_open())
            return;
        _outputFile << log_message << END_LINE;
    }

public:
    Logger() = delete;

    ~Logger() = delete;

    Logger operator=(const Logger &logger) = delete;

    /**
     * <br>Sets the output file full path.<br><br>
     * If output file is set and is valid, logging to file will automatically be done.<br>
     * Set to an empty string to not save logs to file. (Calling this function without passing anything will have this effect)<br><br>
     * Examples:
     * <br> SetOutputFile(std::string(args[0]) + "/log.txt");
     * <br> SetOutputFile(); //Resets the output file
     * @param outputFile Full file path.
     */
    static bool SetOutputFile(const std::string &outputFile = "") {
        if (outputFile.empty()) {
            _outputFile.close();
            return true;
        }
        if (_outputFile.is_open())
            _outputFile.close();
        if (std::filesystem::exists(outputFile)) {
            if (std::filesystem::is_regular_file(outputFile)) {
                _outputFile.open(outputFile, std::ios::app);
                return _outputFile.is_open();
            }
            return false;
        }
        std::filesystem::path outputFilePath(outputFile);
        if (outputFilePath.filename().empty())
            return false;
        outputFilePath.remove_filename();
        std::filesystem::create_directories(outputFilePath);
        _outputFile.open(outputFile, std::ios::app);
        return _outputFile.is_open();
    }

    static bool isOutputFileSet() {
        return _outputFile.is_open();
    }

    static void EnableTimeLogging() {
        _logTime = true;
    }

    static void DisableTimeLogging() {
        _logTime = false;
    }

    static bool isLoggingTime() {
        return _logTime;
    }

    template<typename T>
    static void LogError(T const &log_text) {
        Logger::Log(LogLevel::ErrorLog, log_text);
    }

    template<typename T>
    static void LogWarning(T const &log_text) {
        Logger::Log(LogLevel::WarningLog, log_text);
    }

    template<typename T>
    static void LogInfo(T const &log_text) {
        Logger::Log(LogLevel::InfoLog, log_text);
    }
};

#endif //LOGGER_LOGGER_H
